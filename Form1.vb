﻿Imports CefSharp
Imports CefSharp.WinForms

Public Class Form1
    Private _browser As ChromiumWebBrowser

    Private Sub InitializeChromium()
        Dim settings As New CefSettings
        CefSharp.Cef.Initialize(settings)

        _browser = New ChromiumWebBrowser("http://google.com")

        AddHandler _browser.FrameLoadStart, AddressOf BrowserOnFrameLoadStart
        AddHandler _browser.FrameLoadEnd, AddressOf BrowserOnFrameLoadEnd

        Me.Controls.Add(_browser)
        _browser.BringToFront()
        _browser.Dock = DockStyle.Fill
    End Sub

    Private Sub BrowserOnFrameLoadStart(sender As Object, e As FrameLoadStartEventArgs)
        ProgressBar1.Invoke(
            Sub()
                ProgressBar1.Style = ProgressBarStyle.Marquee
                ProgressBar1.Refresh()
            End Sub
            )

        TextBox1.Invoke(
            Sub()
                TextBox1.Text = _browser.Address
            End Sub)
    End Sub

    Private Sub BrowserOnFrameLoadEnd(sender As Object, e As FrameLoadEndEventArgs)
        ProgressBar1.Invoke(
            Sub()
                ProgressBar1.Style = ProgressBarStyle.Blocks
                ProgressBar1.Refresh()
            End Sub
            )

        TextBox1.Invoke(
            Sub()
                TextBox1.Text = _browser.Address
            End Sub)
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        CefSharp.Cef.Shutdown()
        If _browser IsNot Nothing Then
            ' menghindari memory leak
            RemoveHandler _browser.FrameLoadStart, AddressOf BrowserOnFrameLoadStart
            RemoveHandler _browser.FrameLoadEnd, AddressOf BrowserOnFrameLoadEnd

            ' membersihkan sampah memory
            _browser.Dispose()
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitializeChromium()
    End Sub

    Private Sub BtnGo_Click(sender As Object, e As EventArgs) Handles BtnGo.Click
        If _browser IsNot Nothing Then
            _browser.Load(TextBox1.Text.Trim())
        End If
    End Sub

    Private Sub BtnGoBack_Click(sender As Object, e As EventArgs) Handles BtnGoBack.Click
        If _browser IsNot Nothing AndAlso _browser.CanGoBack Then
            _browser.Back()
        End If
    End Sub

    Private Sub BtnGoForward_Click(sender As Object, e As EventArgs) Handles BtnGoForward.Click
        If _browser IsNot Nothing AndAlso _browser.CanGoForward Then
            _browser.Forward()
        End If
    End Sub
End Class
