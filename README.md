[![CefSharp Logo](http://cefsharp.github.io/images/logo2.png)](https://cefsharp.github.io/ "CefSharp - Embedded Chromium for .NET")

## Links

- [CefSharp](http://cefsharp.github.io/)
- [WinForms NuGet](https://www.nuget.org/packages/CefSharp.WinForms/)
- [CEF Bitbucket Project](https://bitbucket.org/chromiumembedded/cef/overview) : The official CEF issue tracker
- [CEF Forum](http://magpcss.org/ceforum/) : The official CEF Forum
- [CEF API Docs](http://magpcss.org/ceforum/apidocs3/index-all.html) : Well worth a read if you are implementing a new feature
- [CefSharp API Doc](http://cefsharp.github.io/api/)

## Documentation

* See the [CefSharp.Wpf.Example](https://github.com/cefsharp/CefSharp/tree/master/CefSharp.Wpf.Example) or [CefSharp.WinForms.Example](https://github.com/cefsharp/CefSharp/tree/master/CefSharp.WinForms.Example) projects for example web browsers built with CefSharp. They demo most of the available features.
* See the [CefSharp.MinimalExample](https://github.com/cefsharp/CefSharp.MinimalExample/) project for a basic demo of using the CefSharp NuGet packages.
* See the [General Usage Guide](https://github.com/cefsharp/CefSharp/wiki/General-Usage) in help getting started/dealing with common scenarios.
* See the [Wiki](https://github.com/cefsharp/CefSharp/wiki) for work-in-progress documentation
* See the [FAQ](https://github.com/cefsharp/CefSharp/wiki/Frequently-asked-questions) for help with common issues
* Upgrading from an earlier version of CefSharp? See the [ChangeLog](https://github.com/cefsharp/CefSharp/wiki/ChangeLog) for breaking changes and upgrade tips.
* [CefSharp API](https://cefsharp.github.io/api/55.0.0/) generated from the source code comments.

## YouTube Tutorial - VB.Net + CefSharp (ChromiumEmbeddedBrowser)
[![IMAGE ALT TEXT HERE](https://i9.ytimg.com/vi/6d-AgfFzr70/mqdefault.jpg?time=1563433801527&sqp=CMi2wOkF&rs=AOn4CLDZuvxgDxetaKZuIyGsMTve8DqBGg)](https://youtu.be/6d-AgfFzr70)